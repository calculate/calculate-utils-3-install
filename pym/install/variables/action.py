# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import ActionVariable

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_install3', sys.modules[__name__])

ServerSetup = "server_setup"

class VariableAcInstallMerge(ActionVariable):
    """
    Action variable which has value "on" 
    in ebuild phase preinst or reconfigure system
    """

    def action(self, cl_action):
        if (cl_action in ("system", "merge") and
                    self.Get('os_install_root_type') != 'flash' and
                    self.Get('os_install_pxe') == 'off' and
                    self.Get('cl_live') == 'off' or
                        cl_action in (ServerSetup, "sync",
                                      "domain", "undomain",)
                and self.Get('cl_merge_pkg')):
            return "on"
        return "off"


class VariableAcInstallLive(ActionVariable):
    """
    Action variable which has value "on"
    in configure system, install system hdd,  and postinst ebuild phase
    only not chroot
    """
    nonchroot = True

    def action(self, cl_action):
        if (cl_action in ("system", "merge") and
                    self.Get('os_install_root_type') != 'flash' and
                    self.Get('os_install_pxe') == 'off' or
                        cl_action in (ServerSetup,
                                      "sync", "domain", "undomain",)
                and self.Get('cl_merge_pkg')):
            return "on"
        return "off"


class VariableAcInstallConfig(ActionVariable):
    """
    Action variable which has value "on" for emerge --config
    cl-config
    """
    def action(self, cl_action):
        if cl_action in ("config",):
            return "on"
        return "off"


class VariableAcInstallDisk(ActionVariable):
    """
    Action variable which has value "on" for installation on hdd
    """

    def action(self, cl_action):
        if (cl_action == 'system' and
                    self.Get('os_install_root_type') != "flash" and
                    self.Get('os_install_pxe') != "on"):
            return "on"
        else:
            return "off"


class VariableAcInstallFlash(ActionVariable):
    """
    Action variable which has value "on" for USB flash
    """

    def action(self, cl_action):
        if (cl_action == 'system' and
                    self.Get('os_install_root_type') == 'flash'):
            return "on"
        return "off"


class VariableAcInstallPxe(ActionVariable):
    """
    Action variable which has value "on" for PXE installation
    """

    def action(self, cl_action):
        if cl_action == 'system' and self.Get('os_install_pxe') == 'on':
            return "on"
        return "off"


class VariableAcInstallConfigure(ActionVariable):
    """
    Action variable which has value "up" for configuration
    """

    def action(self, cl_action):
        cl_setup = self.Get('cl_setup')
        if cl_action == "merge" and cl_setup:
            return "on"
        return "off"


class VariableAcInstallUnmerge(ActionVariable):
    """
    Action variable which has value "up" on prerm ebuild phase
    """

    def action(self, cl_action):
        if (cl_action == "merge" and
                    self.Get('cl_ebuild_phase') in ('prerm', 'postrm')):
            return "on"
        return "off"


class VariableAcInstallPatch(ActionVariable):
    """
    Action variable which has value "on" 
    in ebuild phase preinst or reconfigure system
    """

    def action(self, cl_action):
        if cl_action in ("patch",):
            return "on"
        return "off"
