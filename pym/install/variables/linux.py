# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import ReadonlyVariable, VariableInterface
from calculate.lib.variables.linux import Linux

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_install3', sys.modules[__name__])


class InstallLinux(Linux, VariableInterface):
    def __getFromImageOrCurrent(self, currentVar):
        """Get value from install image or current system"""
        if self.Get('cl_action') == 'system':
            image = self.Get('cl_image')
            if image:
                d = image.getInfo()
                # support lazy values
                res = d.get(currentVar, "")
                return str(res()) if callable(res) else res
            else:
                return ""
        else:
            return self.Get(currentVar)

    # variable for get current system info (example: os_linux_shortname)
    current_variable = ""
    # field of distroinfo (name,ver,build,march and etc)
    distroinfo_field = ""

    def get(self):
        """Get by distroinfo or current info"""
        return self.__getFromImageOrCurrent(self.current_variable)

class VariableOsInstallChrootableSet(InstallLinux, ReadonlyVariable):
    """Можно ли выполнить chroot в систему"""
    current_variable = "os_chrootable_set"

class VariableOsChrootableSet(ReadonlyVariable):
    """Можно ли выполнить chroot в систему"""
    type = "boolean"
    value = "on"

class VariableOsInstallLinuxShortname(InstallLinux, ReadonlyVariable):
    """Shortname of system"""
    current_variable = "os_linux_shortname"


class VariableOsInstallLinuxVer(InstallLinux, ReadonlyVariable):
    """Version of system"""
    current_variable = "os_linux_ver"


class VariableOsInstallLinuxBuild(InstallLinux, ReadonlyVariable):
    """Build of system"""
    current_variable = "os_linux_build"


class VariableOsInstallArchMachine(InstallLinux, ReadonlyVariable):
    """Arch of system"""
    current_variable = "os_arch_machine"


class VariableOsInstallLinuxFiles(InstallLinux, ReadonlyVariable):
    """Files num in system"""
    current_variable = "os_linux_files"


class VariableOsInstallLinuxName(InstallLinux, ReadonlyVariable):
    """
    Install distro name
    """
    current_variable = "os_linux_name"


class VariableOsInstallLinuxSystem(InstallLinux, ReadonlyVariable):
    """
    Install system name
    """
    current_variable = "os_linux_system"


class VariableOsInstallLinuxSubname(InstallLinux, ReadonlyVariable):
    """
    Install subname
    """
    current_variable = "os_linux_subname"
