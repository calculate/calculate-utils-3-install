# -*- coding: utf-8 -*-

# Copyright 2008-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.lib.datavars import (Variable, VariableInterface,
                                    ReadonlyVariable, ReadonlyTableVariable,
                                    FieldValue, HumanReadable)
from calculate.lib.utils import device

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_install3', sys.modules[__name__])

class LvmHelper(VariableInterface):
    def getLvmData(self):
        for vg, lv, pv in device.lvm.pvdisplay_full():
            yield lv, vg, pv

#######################################################
# Devices variables
#######################################################
class VariableOsLvmData(ReadonlyTableVariable, LvmHelper):
    """
    Information about LVM
    """
    source = ['os_lvm_lvname',
              'os_lvm_vgname',
              'os_lvm_pvname',
              'os_lvm_pvname_parent'
              ]

    def get(self, hr=HumanReadable.No):
        """LVM hash"""
        def generator():
            for lvname, vgname, pvname in self.getLvmData():
                all_base = device.udev.get_all_base_devices(name=pvname)
                full_base = ",".join(all_base)
                yield lvname, vgname, pvname, full_base
        return list(generator()) or [[]]

    setValue = Variable.setValue


class VariableOsLvmLvname(FieldValue, ReadonlyVariable):
    """
    Logical volumes names
    """
    type = "list"
    source_variable = "os_lvm_data"
    column = 0


class VariableOsLvmVgname(FieldValue, ReadonlyVariable):
    """
    Volume groups names
    """
    type = "list"
    source_variable = "os_lvm_data"
    column = 1


class VariableOsLvmPvname(FieldValue, ReadonlyVariable):
    """
    Phisical volumes names
    """
    type = "list"
    source_variable = "os_lvm_data"
    column = 2

class VariableOsLvmPvnameParent(FieldValue, ReadonlyVariable):
    """
    Phisical volumes names
    """
    type = "list"
    source_variable = "os_lvm_data"
    column = 3
