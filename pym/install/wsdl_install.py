# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys

from calculate.lib.datavars import VariableError, DataVarsError
from .distr import DistributiveError

from . import install
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate, _

setLocalTranslate('cl_install3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)

from calculate.core.server.func import WsdlBase
from calculate.lib.utils.partition import VolumesError
from .utils.cl_install import ClInstallAction
from .utils.cl_setup import (
    ClSetupLocaleAction, ClSetupVideoAction, ClSetupSystemAction,
    ClSetupBootAction, ClSetupNetworkAction, ClSetupAudioAction,
    ClSetupSessionAction, ClSetupThemesAction)


class Wsdl(WsdlBase):
    methods = [{
        # идентификатор метода
        'method_name': "install",
        # категория метода
        'category': __("Installation"),
        # заголовок метода 
        'title': __("System Install"),
        # иконка для графической консоли
        'image': ('calculate-install,system-installer,applications-other,'
                  'drive-harddisk'),
        # метод присутствует в графической консоли
        'gui': True,
        # консольная команда
        'command': 'cl-install',
        # права для запуска метода 
        'rights': ['install'],
        # объект содержащий модули для действия
        'logic': {'Install': install.Install},
        # описание действия
        'action': ClInstallAction,
        # объект переменных
        'datavars': "install",
        'native_error': (VariableError, DistributiveError, VolumesError,
                         DataVarsError, install.InstallError),
        # значения по умолчанию для переменных этого метода
        'setvars': {'cl_action!': 'system',
                    'cl_chroot_status!': 'off', 'cl_install_type': 'hdd',
                    'cl_dispatch_conf': 'usenew'},
        # описание груп (список лямбда функций)
        'groups': [
            lambda group: group(_("Language and locale"),
                                image="welcome",
                                normal=('os_install_locale_lang',
                                        'os_install_clock_timezone'),
                                expert=('os_install_locale_keyboard_layout',
                                        'os_install_clock_type',)),
            lambda group: group(_("Distribution"),
                                normal=('cl_image_filename',),
                                hide=('cl_image_linux_shortname',
                                      'cl_image_arch_machine',
                                      'cl_image_new_only'),
                                expert=('cl_image_linux_shortname',
                                        'cl_image_arch_machine',
                                        'cl_image_new_only')),
            lambda group: group(_("Installation type"),
                                normal=('cl_autopartition_set',),
                                hide=('cl_autopartition_set',
                                      'cl_autopartition_root_size',
                                      'cl_autopartition_root_format',
                                      'cl_autopartition_calculate_format',
                                      'cl_autopartition_swap_size',),
                                brief=('cl_autopartition_brief_set',),
                                expert=('cl_autopartition_scheme',
                                        'cl_autopartition_root_format',
                                        'cl_autopartition_calculate_format',
                                        'cl_autopartition_table',
                                        'cl_autopartition_root_size',
                                        'cl_autopartition_swap_size',
                                        'cl_autopartition_device'),
                                expert_label=_(
                                    "Click to select partitions to be created")
                                ),
            lambda group: group(_("Layout"),
                                normal=('os_location_data',),
                                hide=('os_location_data', 'os_install_mbr',
                                      'os_install_uefi'),
                                brief_force=('os_location_brief_data',
                                             'os_install_bootloader'),
                                brief=('os_install_uefi',),
                                expert=('cl_uuid_set',
                                        'os_install_mbr',
                                        'os_install_uefi')),
            lambda group: group(_("Network settings"),
                                normal=(
                                    'cl_network_migrate_set',),
                                expert=('os_install_net_conf',
                                    'os_install_net_data',
                                    'os_install_net_fqdn', 'os_install_ntp',
                                    'os_install_net_dns',
                                    'os_install_net_dns_search',
                                    'os_install_net_route_data'),
                                expert_label=_(
                                    "Click to select network settings")
                                ),
            lambda group: group(_("Users"),
                                normal=(
                                    'cl_migrate_root_pwd_plain',
                                    'cl_grub_passwd_set',
                                    'cl_migrate_data',
                                    'cl_autologin'),
                                expert=('cl_install_home_crypt_set',),
                                hide=('cl_migrate_data',),
                                brief=('cl_migrate_data_brief',)),
            lambda group: group(_("Audio"),
                                    normal=('os_audio',),
                                    expert=('os_audio_default',)),
            lambda group: group(_("Video"),
                                normal=('os_install_x11_video_drv',
                                        'os_install_x11_composite',
                                        'os_install_x11_resolution_preferred',
                                        'os_install_grub_terminal'),
                                expert=('os_install_fb_resolution_preferred',)),
            lambda group: group(_("Update"),
                                normal=('cl_install_autocheck_set',
                                        'cl_install_autocheck_interval',
                                        'cl_install_cleanpkg_set',
                                        'cl_install_other_set'),
                                expert=('cl_install_update_pkg_set',))],
        # действие выводит информацию перед запуском
        'brief': {'next': __("Run"),
                  'image': 'finish',
                  'name': __("Start installing")}},
        # установка на Flash
        {
            'method_name': "install_flash",
            'category': __("Installation"),
            'title': __("Flash Install"),
            'image': ('calculate-install-flash,'
                      'drive-removable-media-usb-pendrive,'
                      'drive-removable-media-usb,media-flash'),
            'command': 'cl-install-flash',
            'gui': True,
            'rights': ['install'],
            'logic': {'Install': install.Install},
            'action': ClInstallAction,
            'datavars': "install",
            'native_error': (VariableError, DistributiveError, VolumesError,
                             DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'system',
                        'cl_chroot_status!': 'off',
                        'cl_install_type': 'flash',
                        'cl_protect_use_set!': 'off',
                        'cl_autopartition_set!': 'off',
                        'cl_dispatch_conf': 'usenew'},
            'groups': [
                lambda group: group(_("Flash install"),
                                    normal=('cl_image_filename',
                                            'os_install_disk_single',
                                            'cl_target_fs',
                                            'os_install_format_single_set'),
                                    next_label=_("Run"))],
            'brief': {'next': __("Run"),
                      'name': __("Start installing")}},
        # PXE установка
        {
            'method_name': "install_pxe",
            'category': __("Installation"),
            'title': __("PXE Install"),
            'image': ('calculate-install-pxe,gnome-network-properties,'
                      'network-server,'
                      'preferences-desktop-remote-desktop'),
            'command': 'cl-install-pxe',
            'gui': True,
            'rights': ['installpxe'],
            'logic': {'Install': install.Install},
            'action': ClInstallAction,
            'datavars': "install",
            'native_error': (VariableError, DistributiveError,
                             DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'system', 'os_install_pxe': 'on',
                        'cl_protect_use_set!': 'off',
                        'cl_dispatch_conf': 'usenew'},
            # действие выводит информацию перед запуском
            'brief': {'next': __("Installation"),
                      'image': 'finish',
                      'name': __("Start installing")},
            'groups': [
                lambda group: group(_("PXE install"),
                                    normal=('cl_image_filename',),
                                    expert=('os_install_pxe_path',
                                            'os_install_pxe_ip'),
                                    next_label=_("Run"))]},
        # настройка загрузки системы
        {
            'method_name': "setup_boot",
            'category': __("Configuration"),
            'title': __("Boot"),
            'image': 'calculate-setup-boot,stock_save,drive-harddisk',
            'command': 'cl-setup-boot',
            'gui': True,
            'rights': ['setupboot'],
            'logic': {'Install': install.Install},
            'action': ClSetupBootAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'boot'},
            'groups': [
                lambda group: group(_("Boot"),
                                    normal=(
                                        'os_install_mbr',
                                        'os_install_uefi',
                                        'os_install_grub_terminal',
                                        'cl_grub_pwd',
                                        'cl_grub_remove_pwd_set',
                                    ),
                                    expert=(
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # настройка сети
            'method_name': "setup_network",
            'category': __("Configuration"),
            'title': __("Network"),
            'image': 'calculate-setup-network,network-workgroup,'
                     'network-idle,preferences-system-network',
            'command': 'cl-setup-network',
            'gui': True,
            'rights': ['setupnetwork'],
            'logic': {'Install': install.Install},
            'action': ClSetupNetworkAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_network_migrate_set': 'off',
                        'cl_merge_set!': "on", 'cl_setup': 'network'},
            'groups': [
                lambda group: group(_("Network"),
                                    normal=('os_install_net_conf',
                                            'os_install_net_data',
                                            'os_install_net_fqdn',
                                            'os_install_ntp'),
                                    expert=('os_install_net_dns',
                                            'os_install_net_dns_search',
                                            'os_install_net_route_data',
                                            'cl_templates_locate',
                                            'cl_dispatch_conf',
                                            'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # перенастройка системы
            'method_name': "setup_system",
            'category': __("Configuration"),
            'title': __("System"),
            'image': 'calculate-setup-system,run-build,applications-ide,'
                     'system-run,system,computer',
            'command': 'cl-setup-system',
            'gui': True,
            'rights': ['setupsystem'],
            'logic': {'Install': install.Install},
            'action': ClSetupSystemAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_live': 'off'},
            'groups': [
                lambda group: group(_("Update system settings"),
                                    normal=('cl_live',
                                            'cl_network_configure_set'),
                                    expert=(
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # настройка видео
            'method_name': "setup_video",
            'category': __("Configuration"),
            'title': __("Video"),
            'image': 'calculate-setup-video,system-config-display,'
                     'video-display,gnome-multimedia',
            'command': 'cl-setup-video',
            'gui': True,
            'rights': ['setupvideo'],
            'logic': {'Install': install.Install},
            'action': ClSetupVideoAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'video'},
            'groups': [
                lambda group: group(
                    _("Video"),
                    normal=('os_install_x11_video_drv',
                            'os_install_x11_resolution_preferred',
                            'os_install_x11_composite'),
                    expert=(
                        'os_install_fb_resolution_preferred',
                        'cl_templates_locate',
                        'cl_dispatch_conf',
                        'cl_verbose_set'),
                    next_label=_("Save"))]},
        {
            # настройка звука
            'method_name': "setup_audio",
            'category': __("Configuration"),
            'title': __("Audio"),
            'image': 'calculate-setup-audio,audio-card',
            'command': 'cl-setup-audio',
            'gui': True,
            'rights': ['setupaudio'],
            'logic': {'Install': install.Install},
            'action': ClSetupAudioAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'audio'},
            'groups': [
                lambda group: group(_("Audio"),
                                    normal=('os_audio',),
                                    expert=(
                                        'os_audio_default',
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # настройка локали
            'method_name': "setup_locale",
            'category': __("Configuration"),
            'title': __("Locale"),
            'image': 'calculate-setup-locale,locale,preferences-desktop-locale',
            'command': 'cl-setup-locale',
            'gui': True,
            'rights': ['setuplocale'],
            'logic': {'Install': install.Install},
            'action': ClSetupLocaleAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'locale'},
            'groups': [
                lambda group: group(_("Locale"),
                                    normal=('os_install_locale_lang',
                                            'os_install_clock_timezone'),
                                    expert=(
                                        'os_install_locale_keyboard_layout',
                                        'os_install_clock_type',
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # настройка локали
            'method_name': "setup_session",
            'category': __("Configuration"),
            'title': __("Session"),
            'image': 'calculate-setup-session,system-lock-screen',
            'command': 'cl-setup-session',
            'gui': True,
            'rights': ['setupsession'],
            'logic': {'Install': install.Install},
            'action': ClSetupSessionAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'session'},
            'groups': [
                lambda group: group(_("Session"),
                                    normal=('cl_autologin',
                                            'cl_install_home_crypt_set'),
                                    expert=(
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]},
        {
            # настройка тем
            'method_name': "setup_theme",
            'category': None,
            'title': __("Themes"),
            'image': None,
            'command': 'cl-setup-themes',
            'gui': True,
            'rights': ['setupthemes'],
            'logic': {'Install': install.Install},
            'action': ClSetupThemesAction,
            'datavars': "install",
            'native_error': (
                VariableError, DataVarsError, install.InstallError),
            'setvars': {'cl_action!': 'merge', 'cl_merge_pkg!': [None],
                        'cl_merge_set!': "on", 'cl_setup': 'themes'},
            'groups': [
                lambda group: group(_("Session"),
                                    normal=(),
                                    expert=(
                                        'cl_templates_locate',
                                        'cl_dispatch_conf',
                                        'cl_verbose_set'),
                                    next_label=_("Save"))]}
    ]
