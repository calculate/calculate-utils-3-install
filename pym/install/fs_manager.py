# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from os import path

from calculate.lib.cl_lang import setLocalTranslate

setLocalTranslate('cl_install3', sys.modules[__name__])


class FileSystemManager():
    """Convert dict install option"""

    defaultOpt = ['noatime']
    defaultBindOpts = ['bind']
    #Python3 compat problem:
    # dict keys() now sorted based on insertion order,
    # instead of being sorted arbitrary by hash.
    # Just so the order of keys could be the same as 
    # in older version, I swaped some pairs around.
    supportFS = {
        'f2fs': {'defaultopt': defaultOpt,
                'format': '/usr/sbin/mkfs.f2fs',
                'formatparam': '{labelparam} -f {device}',
                'gpt': '8300',
                'label': '-l {labelname}',
                'msdos': '83',
                'ssd': [],
                'compress': None,
                'type': ['hdd', 'usb-hdd']},
        'btrfs': {'defaultopt': defaultOpt,
                  'format': '/sbin/mkfs.btrfs',
                  'formatparam': '{labelparam} -f {device}',
                  'gpt': '8300',
                  'label': '-L {labelname}',
                  'msdos': '83',
                  'ssd': ['ssd'],
                  'type': ['hdd', 'usb-hdd'],
                  'compress': None,
                  'compatible': ['btrfs-compress']},
        'ntfs-3g': {'defaultopt': defaultOpt,
                    'format': '/usr/sbin/mkfs.ntfs',
                    'formatparam': '{labelparam} -FQ {device}',
                    'gpt': '8300',
                    'label': '-L {labelname}',
                    'ssd': [],
                    'auto': False,
                    'msdos': '7',
                    'compress': None,
                    'compatible': ['ntfs']},
        'ntfs': {'defaultopt': defaultOpt,
                 'format': '/usr/sbin/mkfs.ntfs',
                 'formatparam': '{labelparam} -FQ {device}',
                 'gpt': '8300',
                 'label': '-L {labelname}',
                 'msdos': '7',
                 'auto': False,
                 'ssd': [],
                 'compress': None,
                 'compatible': ['ntfs-3g']},
        'xfs': {'defaultopt': defaultOpt,
                'format': '/sbin/mkfs.xfs',
                'formatparam': '{labelparam} -f {device}',
                'gpt': '8300',
                'label': '-L {labelname}',
                'msdos': '83',
                'boot': '-i sparce=0',
                'ssd': [],
                'compress': None,
                'type': ['hdd', 'usb-hdd']},
        'btrfs-compress': {'defaultopt': defaultOpt,
                  'format': '/sbin/mkfs.btrfs',
                  'orig': 'btrfs',
                  'compress': "compress=%s",
                  'formatparam': '{labelparam} -f {device}',
                  'gpt': '8300',
                  'label': '-L {labelname}',
                  'msdos': '83',
                  'ssd': ['ssd'],
                  'type': ['hdd', 'usb-hdd'],
                  'compatible': ['btrfs']},
        'ext4': {'defaultopt': defaultOpt,
                 'format': '/sbin/mkfs.ext4',
                 'formatparam': '{labelparam} {device}',
                 'gpt': '8300',
                 'label': '-L {labelname}',
                 'ssd': [],
                 'msdos': '83',
                 'compress': None,
                 'type': ['hdd', 'usb-hdd']},
        'ext3': {'defaultopt': defaultOpt,
                 'format': '/sbin/mkfs.ext3',
                 'formatparam': '{labelparam} {device}',
                 'gpt': '8300',
                 'label': '-L {labelname}',
                 'ssd': [],
                 'msdos': '83',
                 'compress': None,
                 'type': ['hdd', 'usb-hdd']},
        'ext2': {'defaultopt': defaultOpt,
                 'format': '/sbin/mkfs.ext2',
                 'formatparam': '{labelparam} {device}',
                 'gpt': '8300',
                 'label': '-L {labelname}',
                 'ssd': [],
                 'msdos': '83',
                 'compress': None,
                 'type': ['hdd', 'usb-hdd']},
        'uefi': {'defaultopt': defaultOpt,
                 'format': '/usr/sbin/mkfs.vfat',
                 'formatparam': '{labelparam} -F 32 {device}',
                 'gpt': 'EF00',
                 'label': '-n {labelname}',
                 'msdos': '0b',
                 'ssd': [],
                 'auto': False,
                 'compress': None,
                 'type': ['hdd']},
        'vfat': {'defaultopt': defaultOpt,
                 'format': '/usr/sbin/mkfs.vfat',
                 'formatparam': '{labelparam} -F 32 {device}',
                 'gpt': '0700',
                 'label': '-n {labelname}',
                 'msdos': '0b',
                 'auto': False,
                 'ssd': [],
                 'compress': None,
                 'type': ['flash']},
        'jfs': {'defaultopt': defaultOpt,
                'format': '/sbin/mkfs.jfs',
                'formatparam': '{labelparam} -f {device}',
                'gpt': '8300',
                'label': '-L {labelname}',
                'msdos': '83',
                'ssd': [],
                'compress': None,
                'type': ['hdd', 'usb-hdd']},
#        'nilfs2': {'defaultopt': defaultOpt,
#                   'format': '/sbin/mkfs.nilfs2',
#                   'formatparam': '{labelparam} {device}',
#                   'gpt': '8300',
#                   'label': '-L {labelname}',
#                   'msdos': '83',
#                   'ssd': [],
#                   'type': ['hdd', 'usb-hdd']},
        'swap': {'defaultopt': ['sw'],
                 'format': '/sbin/mkswap',
                 'formatparam': '{device}',
                 'gpt': '8200',
                 'label': '',
                 'ssd': [],
                 'auto': False,
                 'compress': None,
                 'msdos': '82'},
        }

    default_param = {'defaultopt': defaultOpt,
                     'gpt': '8300',
                     'msdos': '83',
                     'compress': None,
                     'ssd': []}

    @classmethod
    def firstAvailable(cls, listFS):
        for fs in listFS:
            if path.exists(cls.supportFS['format']):
                return fs
        else:
            return ""

    @classmethod
    def get_default_fs(cls, dv, installtype):
        if installtype == 'flash':
            return 'vfat'
        filesystems = dv.Get('install.cl_install_fs')
        for fs in filesystems:
            if fs in cls.supportFS and path.exists(cls.supportFS[fs]['format']):
                return fs
        return 'ext3'

    @classmethod
    def getDefaultOpt(cls, fs, ssd=False, compress=None):
        fsopts = cls.supportFS.get(fs, cls.default_param)
        return ",".join(fsopts['defaultopt'] +
                        (fsopts['ssd'] if ssd else []) +
                        ([fsopts['compress'] % compress]
                         if fsopts['compress'] and compress else []))

    @classmethod
    def checkFSForTypeMount(cls, fs, roottype, mp):
        if mp.startswith('/boot/efi'):
            if fs not in ('uefi', 'vfat'):
                return False
            else:
                return True
        return roottype in cls.supportFS.get(fs, {}).get('type', [])
